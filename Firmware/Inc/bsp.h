/**
 * @file   	bsp.h
 * @author 	Hassan Abid
 * @version	v7.0.0
 * @date	Oct 25, 2016
 * 
 * @brief   Board Support Package File for Cowlar Node v7 Hardware.
 */

#ifndef BSP_H_
#define BSP_H_

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>


#include "spi.h"
#define wait_ms HAL_Delay
extern SPI_HandleTypeDef hspi1;
#define SPI_SX1262_Handle   hspi1



extern void printDEBUG_function(const char* format, ...);

#define printd(...)         \
printDEBUG_function("[%d]",  osKernelSysTick());\
printDEBUG_function(__VA_ARGS__)

//#define ADV_DEBUG

/** @}*/

#endif /* BSP_H_ */
