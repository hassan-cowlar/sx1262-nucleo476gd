/*
  ______                              _
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
    (C)2016 Semtech

Description: PingPong, PER demo implementation.

Maintainer: Gregory Cristian & Gilbert Menth
*/

#include "bsp.h"
#include "radio.h"
#include "sx126x-hal.h"
#include "DemoApplication.h"
#include <math.h>

/*!
 * \brief Defines the local payload buffer size
 */
#define BUFFER_SIZE                     255


/*!
 * \brief Define time used in PingPong demo to synch with cycle
 * RX_TIMEOUT_MARGIN is the free time between each cycle (time reserve)
 */
#define RX_TIMEOUT_MARGIN               150  // ms
#define RX_TX_TRANSITION_WAIT           5    // ms



#define LoRa_clearIRQFlags( flags ) \
	RadioStatus.Irq &= ~(flags)


/*!
 * \brief Buffer and its size
 */
uint8_t BufferSize = BUFFER_SIZE;
uint8_t LoRa_Buffer[BUFFER_SIZE];




/*!
 * \brief Function to be executed on Radio Tx Done event
 */
void OnTxDone( void );

/*!
 * \brief Function to be executed on Radio Rx Done event
 */
void OnRxDone( void );

/*!
 * \brief Function executed on Radio Tx Timeout event
 */
void OnTxTimeout( void );

/*!
 * \brief Function executed on Radio Rx Timeout event
 */
void OnRxTimeout( void );

/*!
 * \brief Function executed on Radio Rx Error event
 */
void OnRxError( IrqErrorCode_t );

/*!
 * \brief Function executed on Radio CAD Done event
 */
void OnCadDone( bool channelActivityDetected );

/*!
 * \brief All the callbacks are stored in a structure
 */
RadioCallbacks_t RadioEvents =
{
    &OnTxDone,        // txDone
    &OnRxDone,        // rxDone
    NULL,             // rxPreambleDetect
    NULL,             // rxSyncWordDone
    NULL,             // rxHeaderDone
    &OnTxTimeout,     // txTimeout
    &OnRxTimeout,     // rxTimeout
    &OnRxError,       // rxError
    &OnCadDone,       // cadDone
};

struct {
	
	uint32_t Irq;
	
	uint32_t lastTxTick;
	uint32_t lastRxTick;
	
	int8_t RSSI;
	int8_t SNR;

} RadioStatus;

// SPI
// mosi, miso, sclk, nss, dio0, dio1, dio2, dio3, rst, freqSel, deviceSel, antSwPower, callbacks...
SX126xInterface_t *Radio = 0;


/*!
 * \brief Mask of IRQs
 */
uint16_t IrqMask = 0x0000;

/*!
 * \brief Locals parameters and status for radio API
 * NEED TO BE OPTIMIZED, COPY OF STUCTURE ALREADY EXISTING
 */
PacketParams_t PacketParams;
PacketStatus_t PacketStatus;
ModulationParams_t ModulationParams;


/*!
 * \brief Frequency Error (only LSB)
 */
static double FreErrorLsb = 0.0;




void LoRa_config( uint8_t modulation );


uint8_t LoRa_sleepMode(void)
{
	SleepParams_t sleepParams;
	sleepParams.Value = 0;
	sleepParams.Fields.WarmStart = true;
	
	Radio->SetSleep(sleepParams);
	return 0;
}

uint8_t LoRa_standbyMode(void)
{
	Radio->SetStandby(STDBY_RC);
	return 0;
}


uint8_t LoRa_receiverMode(uint32_t timeout, bool boostedRx)
{
  if (Radio->GetOperatingMode() == MODE_RX)
    return 0;
  
	LoRa_standbyMode();
	
	IrqMask = IRQ_RX_DONE | IRQ_CRC_ERROR | IRQ_RX_TX_TIMEOUT;
  Radio->SetDioIrqParams( IrqMask, IrqMask, IRQ_RADIO_NONE, IRQ_RADIO_NONE );
  
	Radio->ClearIrqStatus(IRQ_RADIO_ALL);
	RadioStatus.Irq = 0;
	
	
	//Timeout shifted by 6 to convert into milliseconds.
	// timeout duration = value * 15.625 us
	if( boostedRx == false )
	{
			Radio->SetRx( timeout << 6 );
	}
	else
	{
			Radio->SetRxBoosted( timeout << 6 );
	}
}

__inline uint8_t LoRa_IrqHandler(void)
{
	Radio->ProcessIrqs();
	return 0;
}


uint8_t LoRa_TransmitPacket(uint8_t* data, uint32_t length)
{
	if (length > 255)
		return -1;
	
	LoRa_standbyMode();
	IrqMask = IRQ_TX_DONE | IRQ_RX_TX_TIMEOUT;
	Radio->SetDioIrqParams( IrqMask, IrqMask, IRQ_RADIO_NONE, IRQ_RADIO_NONE );
	
	//clear transmit interrupt flags before going into transmit mode.
	Radio->ClearIrqStatus(IRQ_RADIO_ALL);
	RadioStatus.Irq = 0;
	
  Radio->SetInterruptMode();
	// timeout = 5 seconds. 
	//  shifted by 6 because timeout duration = value * 15.625 us
	Radio->SendPayload( data, length, 5000 << 6 );

	///@todo : wait for tx done or timeout interrupt before returning.
	while (!(RadioStatus.Irq & (IRQ_TX_DONE | IRQ_TX_TIMEOUT)))
	{}
	
	///@todo : add interrupt handling, and implement callbacks.
	Radio->SetPollingMode();
  LoRa_sleepMode();
	LoRa_clearIRQFlags( IRQ_TX_DONE | IRQ_TX_TIMEOUT );
	return 0;
}







const SX126xHalInterface_t RadioHal = {

    .hspiSX         =   &SPI_SX1262_Handle,
    .RadioNss       =   {.port =  SPI1_NSS_GPIO_Port, .pin  = SPI1_NSS_Pin },
    .RadioReset     =   {.port =  RST_GPIO_Port, .pin  = RST_Pin },
    .BUSY           =   {.port =  DIO0_GPIO_Port, .pin  = DIO0_Pin },
    .DIO1           =   {.port =  DIO1_GPIO_Port, .pin  = DIO1_Pin,  .IRQn = DIO1_EXTI_IRQn},
    .DIO2           =   {.port =  NULL,  .pin  = NULL },
    .DIO3           =   {.port =  NULL,  .pin  = NULL },
    .MISO           =   {.port =  GPIOA, .pin  = GPIO_PIN_6 },
    .MOSI           =   {.port =  GPIOA, .pin  = GPIO_PIN_7 },
    .SCK            =   {.port =  GPIOA, .pin  = GPIO_PIN_5 },
    .antSwitchPower =   {.port =  antSwPower_GPIO_Port, .pin  = antSwPower_Pin }
};
void LoRa_Init( void )
{
    
    Radio = SX126xHalInit(&RadioHal, &RadioEvents);

    // Can also be set in LDO mode but consume more power
    Radio->SetRegulatorMode(  USE_DCDC );
    Radio->SetStandby( STDBY_RC );

    memset( &LoRa_Buffer, 0x00, BufferSize );
        
    
    Eeprom.EepromData.ModulationParams.PacketType = PACKET_TYPE_LORA;
    Eeprom.EepromData.PacketParams.PacketType     = PACKET_TYPE_LORA;
    

		LoRa_config( PACKET_TYPE_LORA );

}


/*
 * Function still being implemented >>> To be completed 
 * WARNING: Computation is in float and his really slow
 * LongInterLeaving vs LegacyInterLeaving has no influence on TimeOnAir.
 */
uint32_t LoRa_getTimeOnAir( uint8_t modulation )
{
    uint16_t result = 2000;
    uint8_t LowDatarateOptimize = 0;

    if( modulation == PACKET_TYPE_LORA )
    {
        volatile double loraBw = 0.0;
        volatile double FreqErrorUnits = 0.0;
        
        switch( Eeprom.EepromData.ModulationParams.Params.LoRa.Bandwidth )
        {
            case LORA_BW_500:
                loraBw = 500e3;
                break;

            case LORA_BW_250:
                loraBw = 250e3;
                break;

            case LORA_BW_125:
                loraBw = 125e3;
                break;

            case LORA_BW_062:
                loraBw = 62e3;
                break;

            case LORA_BW_041:
                loraBw = 41e3;
                break;

            case LORA_BW_031:
                loraBw = 31e3;
                break;

            case LORA_BW_020:
                loraBw = 20e3;
                break;

            case LORA_BW_015:
                loraBw = 15e3;
                break;

            case LORA_BW_010:
                loraBw = 10e3;
                break;

            case LORA_BW_007:
                loraBw = 7e3;
                break;

            default:
                loraBw = 7e3;
                break;
        }

        /* Used to compute the freq Error */
        FreqErrorUnits = FREQ_ERR;
        FreErrorLsb = FreqErrorUnits * ( ( double )loraBw / 1000 ) / 500;

        float ts = 1 << Eeprom.EepromData.ModulationParams.Params.LoRa.SpreadingFactor; // time for one symbol in ms
              ts = (float)ts / (float)loraBw;
              ts = ts * 1000; // from seconds to miliseconds

        float tPreamble = ( Eeprom.EepromData.PacketParams.Params.LoRa.PreambleLength + 4.25 + ( ( Eeprom.EepromData.ModulationParams.Params.LoRa.SpreadingFactor > 6 ) ? 2 : 0 )) * ts; // time of preamble

        switch( Eeprom.EepromData.ModulationParams.Params.LoRa.Bandwidth )
        {
            case LORA_BW_500:
                break;

            case LORA_BW_250:
                if( Eeprom.EepromData.ModulationParams.Params.LoRa.SpreadingFactor == LORA_SF12 )
                {
                    LowDatarateOptimize = 1;
                }
                break;

            case LORA_BW_125:
                if( Eeprom.EepromData.ModulationParams.Params.LoRa.SpreadingFactor >= LORA_SF11 )
                {
                    LowDatarateOptimize = 1;
                }
                break;

            case LORA_BW_062:
                if( Eeprom.EepromData.ModulationParams.Params.LoRa.SpreadingFactor >= LORA_SF10 )
                {
                    LowDatarateOptimize = 1;
                }
                break;

            case LORA_BW_041:
            case LORA_BW_031:
                if( Eeprom.EepromData.ModulationParams.Params.LoRa.SpreadingFactor >= LORA_SF9 )
                {
                    LowDatarateOptimize = 1;
                }
                break;

            case LORA_BW_020:
            case LORA_BW_015:
                if( Eeprom.EepromData.ModulationParams.Params.LoRa.SpreadingFactor >= LORA_SF8 )
                {
                    LowDatarateOptimize = 1;
                }
                break;

            case LORA_BW_010:
            case LORA_BW_007:
                if( Eeprom.EepromData.ModulationParams.Params.LoRa.SpreadingFactor >= LORA_SF7 )
                {
                    LowDatarateOptimize = 1;
                }
                break;
        }

        float nData = ceil( ( float )( ( 8 * Eeprom.EepromData.PacketParams.Params.LoRa.PayloadLength +                                           \
                       16 * ( ( Eeprom.EepromData.PacketParams.Params.LoRa.CrcMode == LORA_CRC_OFF ) ? 0 : 1 ) +                \
                       ( ( Eeprom.EepromData.PacketParams.Params.LoRa.HeaderType == LORA_PACKET_VARIABLE_LENGTH ) ? 20 : 0 ) -  \
                       ( 4 * Eeprom.EepromData.ModulationParams.Params.LoRa.SpreadingFactor ) + 8 -                             \
                       ( 8 *( ( Eeprom.EepromData.ModulationParams.Params.LoRa.SpreadingFactor > 6 ) ? 1 : 0 ) ) ) / 4 ) );

               nData = ceil( ( float )nData / ( ( float )( Eeprom.EepromData.ModulationParams.Params.LoRa.SpreadingFactor - \
                              ( LowDatarateOptimize * 2 ) ) ) * ( ( Eeprom.EepromData.ModulationParams.Params.LoRa.CodingRate % 4 ) + 4 ) );

        float tPayload = nData * ts;

        float tHeader = 8 * ts;
        // Time on air [ms]
        float ToA = ceil( tPreamble + tPayload + tHeader );

        result = ( uint16_t )ToA + ( ( uint16_t )ToA >> 1 );   // Set some margin
    }
    else if( modulation == PACKET_TYPE_GFSK )
    {
        uint16_t packetBitCount = Eeprom.EepromData.PacketParams.Params.Gfsk.PreambleLength;

        packetBitCount += ( Eeprom.EepromData.PacketParams.Params.Gfsk.SyncWordLength + 1 );
        packetBitCount += Eeprom.EepromData.PacketParams.Params.Gfsk.PayloadLength + 3;
        packetBitCount *= 8;
        // 1500 = 1000 * 1.5 : 1000 for translate s in ms and 1.5 is some margin
        result = ( uint16_t )( ceil( 1500 * ( float )packetBitCount / Eeprom.EepromData.ModulationParams.Params.Gfsk.BitRate ) );
    }
    return result;
}

void LoRa_config( uint8_t modulation )
{
    Radio->SetStandby( STDBY_RC );

    Radio->SetRegulatorMode( USE_LDO );
  
  //The application explicitly calls Radio->processIRQs()
  // to handle incoming interrupts.
    Radio->SetPollingMode();
    
    printd("> InitializeDemoParameters\n\r");
    if( modulation == PACKET_TYPE_LORA )
    {
        printd("set param LORA for demo\n\r");
        ModulationParams.PacketType = PACKET_TYPE_LORA;
        PacketParams.PacketType     = PACKET_TYPE_LORA;

        ModulationParams.Params.LoRa.SpreadingFactor = LORA_SF10;
        ModulationParams.Params.LoRa.Bandwidth       = LORA_BW_500;
        ModulationParams.Params.LoRa.CodingRate      = LORA_CR_4_5;

        PacketParams.Params.LoRa.PreambleLength      = 8;
        PacketParams.Params.LoRa.HeaderType          = LORA_PACKET_VARIABLE_LENGTH;
        PacketParams.Params.LoRa.PayloadLength       = 0xFF;
        PacketParams.Params.LoRa.CrcMode             = LORA_CRC_ON;
        PacketParams.Params.LoRa.InvertIQ            = LORA_IQ_NORMAL;

        
        if( ( ModulationParams.Params.LoRa.SpreadingFactor == LORA_SF6 ) || ( ModulationParams.Params.LoRa.SpreadingFactor == LORA_SF5 ) )
        {
            if( PacketParams.Params.LoRa.PreambleLength < 12 )
            {
                PacketParams.Params.LoRa.PreambleLength = 12;
            }
        }
    }
    else// if( modulation == PACKET_TYPE_GFSK )
    {
        printd("set param GFSK for demo\n\r");
				return ;
    }

    Radio->SetStandby( STDBY_RC );
    Radio->ClearIrqStatus( IRQ_RADIO_ALL );
    Radio->SetPacketType( ModulationParams.PacketType );
    Radio->SetModulationParams( &ModulationParams );
    Radio->SetPacketParams( &PacketParams );

    Radio->SetRfFrequency( 915000000UL );
    Radio->SetBufferBaseAddresses( 0x00, 0x00 );
    
    
    Radio->SetTxParams( SX1262_POWER_TX_MAX, RADIO_RAMP_200_US );

    // only used in GFSK
    Radio->SetSyncWord( ( uint8_t[] ){ 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0 } );
    Radio->SetWhiteningSeed( 0x01FF );

}





int8_t LoRa_getSNR(void)
{
  return RadioStatus.SNR;
}

int8_t LoRa_getRSSI(void)
{
  return RadioStatus.RSSI;
}

RadioOperatingModes_t LoRa_getOperatingMode(void)
{
  return Radio->GetOperatingMode();
}
// ************************     Radio Callbacks     ****************************
// *                                                                           *
// * These functions are called through function pointer by the Radio low      *
// * level drivers                                                             *
// *                                                                           *
// *****************************************************************************
void OnTxDone( void )
{
	//on tx done, the radio automatically goes into
	//standby mode. The explicit call to LoRa_standby is
	//to set the OperatingMode variable.
	LoRa_standbyMode();
	
	RadioStatus.Irq |= IRQ_TX_DONE;
	RadioStatus.lastTxTick = HAL_GetTick();
	
}

void OnRxDone( void )
{
	//on rx done, the radio automatically goes into
	//standby mode. The explicit call to LoRa_standby is
	//to set the OperatingMode variable.
	LoRa_standbyMode();
	
	RadioStatus.Irq |= IRQ_RX_DONE;
	

	Radio->GetPayload( LoRa_Buffer, &BufferSize, BUFFER_SIZE );
	Radio->GetPacketStatus( &PacketStatus );
	
	RadioStatus.RSSI = PacketStatus.Params.LoRa.RssiPkt;
	RadioStatus.SNR = PacketStatus.Params.LoRa.SnrPkt;
}

void OnTxTimeout( void )
{
	//on tx timeout, the radio automatically goes into
	//standby mode. The explicit call to LoRa_standby is
	//to set the OperatingMode variable.
	LoRa_standbyMode();
	
	RadioStatus.Irq |= IRQ_TX_TIMEOUT;
}

void OnRxTimeout( void )
{
	//on rx done, the radio automatically goes into
	//standby mode. The explicit call to LoRa_standby is
	//to set the OperatingMode variable.
	LoRa_standbyMode();
	
	RadioStatus.Irq |= IRQ_RX_TIMEOUT;
}

void OnRxError( IrqErrorCode_t errorCode )
{
		int8_t RSSI, SNR;

    if( errorCode == IRQ_HEADER_ERROR_CODE )
    {
#ifdef ADV_DEBUG
        printd( ">> IRQ_HEADER_ERROR_CODE\n\r" );
#endif
    }
    else if( errorCode == IRQ_SYNCWORD_ERROR_CODE )
    {
#ifdef ADV_DEBUG
        printd( ">> IRQ_SYNCWORD_ERROR_CODE\n\r" );
#endif
    }
    else if( errorCode == IRQ_CRC_ERROR_CODE )
    {
#ifdef ADV_DEBUG
        printd( ">> IRQ_CRC_ERROR_CODE\n\r" );
#endif
    }
    else
    {
#ifdef ADV_DEBUG
        printd( "unknown error\n\r" );
#endif
    }
			
		//clear FIFO by going into the sleep mode.
		LoRa_sleepMode();
		LoRa_standbyMode();
    Radio->GetPacketStatus( &PacketStatus );

		RSSI = PacketStatus.Params.LoRa.RssiPkt;
		SNR = PacketStatus.Params.LoRa.SnrPkt;

}

void OnCadDone( bool channelActivityDetected )
{
	LoRa_standbyMode();
	
	RadioStatus.Irq |= IRQ_CAD_DONE;
    if( channelActivityDetected == true )
    {
			RadioStatus.Irq |= IRQ_CAD_ACTIVITY_DETECTED;
    }
    
}
